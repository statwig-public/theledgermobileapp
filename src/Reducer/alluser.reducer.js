import {ALL_USER} from '../constant'
export default function(state={alluserdata:''}, action){
    switch(action.type){
        
        case ALL_USER:
            return{
                ...state,
                alluserdata:action.payload,
            }
        default:
            return state
    }
}