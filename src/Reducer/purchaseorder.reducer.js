import {
    PURCHASE_ORDER, PURCHASE_ORDER_IDS,
    ADD_PURCHASE_ORDER,
    ADD_PURCHASE_ORDER_IDS
} from '../constant'
export default function (state = { purchaseorderids: [], purchaseorder: [] }, action) {
    switch (action.type) {
        case PURCHASE_ORDER_IDS:
            return {
                ...state,
                purchaseorderids: action.payload.data
            }

        case PURCHASE_ORDER:
            console.log("PURCHASE_ORDER",action.payload);
            
            return {
                ...state,
                purchaseorder: action.payload.data
                //action.payload.data.map(purchaseorderdata => JSON.parse(purchaseorderdata.data)),
            }

        case ADD_PURCHASE_ORDER_IDS:
            let purchaseorderidsdata = state.purchaseorderids
            purchaseorderidsdata.push(action.payload)
            console.log("data",purchaseorderidsdata);
            return {
                ...state,
                purchaseorderids: purchaseorderidsdata
            }

        case ADD_PURCHASE_ORDER:
            let purchaseorderdata = state.purchaseorder
            purchaseorderdata.push({...action.payload})
            console.log("data",purchaseorderdata);
            return {
                ...state,
                purchaseorder: purchaseorderdata
            }

        default:
            return state
    }
}
