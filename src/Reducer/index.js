import { combineReducers } from "redux";
import register from './register.reducer';
import otp from './otp.reducer';
import login from './login.reducer';
import userinfo from './userinfo.reducer';
import loder from './loder.reducer'
import inventory from './inventory.reducer';
import shipment from './shipment.reducer';
import alluser from './alluser.reducer'
import purchaseorder from './purchaseorder.reducer'

export default combineReducers({
    register,
    otp,
    login,
    userinfo,
    inventory,
    loder,
    shipment,
    alluser,
    purchaseorder
})