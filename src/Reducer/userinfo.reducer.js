import {USERINFO,USERUPDATE} from '../constant'
export default function(state={user:{}}, action){
    switch(action.type){
        
        case USERINFO:
            return{
                ...state,
                user:action.payload,
            }
        case USERUPDATE:
            return{
                ...state,
                user:{
                    ...state.user,
                    ...action.payload},
            }
        default:
            return state
    }
}
