import React, { Component } from 'react';
import { View, StatusBar, TextInput, Animated, Image, TouchableOpacity } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class FloatingLabelInput extends Component {
    state = {
        isFocused: false,
    };

    componentWillMount() {
        this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
    }

    handleFocus = () => this.setState({ isFocused: true });
    handleBlur = () => this.setState({ isFocused: false });

    componentDidUpdate() {
        Animated.timing(this._animatedIsFocused, {
            toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
            duration: 200,
        }).start();
    }

    render() {
        const { image, label, ...props } = this.props;
        const labelStyle = {
            position: 'absolute',
            marginLeft: scale(55),
            // top: this._animatedIsFocused.interpolate({
            //     // inputRange: [0, 1],
            //     // outputRange: [40, 10],
            //     inputRange: [0, 1],
            //     outputRange: [0, -2],
            // }),
            fontSize: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: [scale(15), scale(13)],
            }),
            color: this._animatedIsFocused.interpolate({
                inputRange: [0, 1],
                outputRange: ['#707070', '#707070'],
            }),
        };
        return (
            <View style={{ paddingTop: 18, }}>
                <View style={{ flexDirection: "row",width: scale(250),height: scale(50), }}>
                    <Image style={{ position: 'absolute', width: (this.state.isFocused || this.props.value !== '') ? scale(12.24) : scale(16.66), height: (this.state.isFocused || this.props.value !== '') ? scale(12.24) : scale(13), marginLeft: scale(34),marginTop:verticalScale(3) }}
                        resizeMode='contain'
                        source={this.props.image} />
                    
                    <Animated.Text style={labelStyle}>
                        {label}
                    </Animated.Text>

                    
                    <TextInput
                        {...props}
                        style={{ height: scale(50), fontSize: scale(15), borderBottomWidth: 1, borderBottomColor: this.props.error ? "#D80000":"#D6D6D6", marginLeft: scale(34), width: scale(218),paddingTop:scale(20) }}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        keyboardType={this.props.keyboardType}
                        blurOnSubmit
                    />
                    
                    {this.props.password ?
                        <View style={{ alignItems: "flex-end" }}>
                            <TouchableOpacity onPress={this.props.onClick}
                            style={{ position: 'absolute', width: (this.state.isFocused || this.props.value !== '') ? scale(17.17) : scale(16.66), height: (this.state.isFocused || this.props.value !== '') ? scale(10.41) : scale(13.32), }}>
                            <Image style={{ position: 'absolute', width: (this.state.isFocused || this.props.value !== '') ? scale(17.17) : scale(20.66), height: (this.state.isFocused || this.props.value !== '') ? scale(10.41) : scale(13.32), }}
                                resizeMode='cover'
                                source={this.props.passimage} />
                                </TouchableOpacity>
                        </View>
                        : null}
                </View>
                {/* <TextInput
                        {...props}
                        style={{ height: scale(50), fontSize: scale(15), borderBottomWidth: 1, borderBottomColor: "#D6D6D6", marginLeft: scale(34), width: scale(218),paddingTop:scale(20) }}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        keyboardType={this.props.keyboardType}
                        blurOnSubmit
                    /> */}

            </View>
        );
    }
}