import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class Shipment_Order_Card extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.onClick} disabled={this.props.disabled}>
                <View style={{ marginTop: verticalScale(19), width: scale(328), height: scale(127), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                    <View style={{ flexDirection: "row", height: "70%" }}>

                        <View style={{ width: scale(30), marginTop: verticalScale(10), marginLeft: scale(10), borderRadius: 10, height: scale(30) }}>
                            <Image style={{ width: scale(30), height: scale(30) }}
                                resizeMode='center'
                                source={require('../../assets/user.png')} />
                        </View>

                        <View style={{ width: "50%", marginTop: verticalScale(10), marginLeft: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>{this.props.button.deliveryTo}</Text>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Shipment ID</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000" }}>{this.props.button.shipmentId}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Client</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.button.client}</Text>
                            </View>

                        </View>

                        <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), marginRight: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{this.props.button.estimateDeliveryDate}</Text>

                        </View>

                    </View>
                    <View style={{ height: "30%", alignItems: "center" }}>
                        <View style={{ width: scale(308), borderWidth: 1, borderColor: "#F6F6F6", borderRadius: 2, }} />
                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", marginTop: verticalScale(8.5) }}>SHOW MORE</Text>
                    </View>

                </View>
            </TouchableOpacity>
        );
    }
}