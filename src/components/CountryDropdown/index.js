import React, { Component } from 'react';
import { FlatList, Button, View, Text, TextInput, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

export default class CountryDropdown extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isModalVisible}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                }}
                // animationInTiming={1000}
                // animationOutTiming={1000}
                // backdropTransitionInTiming={1000}
                // backdropTransitionOutTiming={1000}
                // backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
            >
                <View style={{ width: scale(250), height: scale(305), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center", marginTop: verticalScale(10) }}>
                    <View style={{ width: scale(200), height: scale(32), backgroundColor: "#fff", borderBottomWidth: 1, borderBottomColor: "#A8A8A8", borderRadius: 8, flexDirection: "row", justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                        <TextInput style={{ width: "80%", }}
                            placeholder="Search"
                            //onChangeText={text => this.SearchFilterFunction(text)}
                            onChangeText={this.props.onChangeText}
                            value={this.props.text}
                        />
                        <Image style={{ width: scale(16.85), height: scale(16.85), marginLeft: scale(15), }}
                            resizeMode='center'
                            source={require('../../assets/Search.png')} />
                    </View>

                    <FlatList
                        data={this.props.countries}
                        extraData={this.state}
                        enableEmptySections={true}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity style={{ width: scale(200), height: scale(25), borderBottomWidth: 1, borderBottomColor: "#DFE6EA", marginTop: verticalScale(10), justifyContent: "center", }}
                                onPress={() => this.props.onPress(item)}>
                                <Text style={{ marginLeft: scale(8) }}>{item}</Text>
                            </TouchableOpacity>
                        )} />
                    <View style={{ height: scale(10) }} />
                </View>
            </Modal>
        );
    }
}