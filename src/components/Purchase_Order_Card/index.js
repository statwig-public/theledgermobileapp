import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class Purchase_Order_Card extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={0.5} onPress={this.props.onClickFunction}>
            <View style={{ marginTop: verticalScale(15), width: scale(328), flex: 1, backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                <View style={{ height: scale(135), position: "relative" }}>
                    <View style={{ flexDirection: "row", height: "70%" }}>

                        <View style={{ width: scale(30), marginTop: verticalScale(10), marginLeft: scale(10), borderRadius: 10, height: scale(30) }}>
                            <Image style={{ width: scale(30), height: scale(30) }}
                                resizeMode='center'
                                source={require('../../assets/user.png')} />
                        </View>

                        <View style={{ width: "50%", marginTop: verticalScale(10), marginLeft: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }} numberOfLines={1}>{this.props.button.client}</Text>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "72%", }}>Purchase Order ID</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000" }}>{this.props.button.orderID}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "72%" }}>Quantity</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.button.total}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "72%", }}>Vendor</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }} numberOfLines={1}>{this.props.button.vendorName}</Text>
                            </View>

                        </View>

                        <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), marginRight: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{this.props.button.date}</Text>

                            {/* <View style={{ width: scale(58), height: scale(24), borderRadius: 12, backgroundColor: (this.props.button.status === "In Transit") ? "#FFB100" : (this.props.button.status === "InTransit") ? "#FFB100" : "#3CB049", alignSelf: "flex-end", marginTop: verticalScale(18), justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#FFFFFF" }}>jhbh</Text>
                            </View> */}

                        </View>

                    </View>
                    <View style={{ alignItems: "center", marginTop: verticalScale(12) }}>

                        <View style={{ alignItems: "center" }}>
                            <View style={{ width: scale(308), borderWidth: 1, borderColor: "#F6F6F6", borderRadius: 2, }} />
                            {/* #F6F6F6 */}
                            {/* <TouchableOpacity activeOpacity={0.5} onPress={this.props.onClickFunction}> */}
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", marginTop: verticalScale(8.5) }}>SHOW MORE</Text>
                            {/* </TouchableOpacity> */}
                        </View>

                    </View>

                </View>
            </View>
            </TouchableOpacity>
        );
    }
}