import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    BackHandler,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class Empty_Card extends Component {
    constructor() {
        super();
        this.state = {

        };
    }

    render() {
        return (
            <View style={{ width: scale(159), height: scale(92), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                <View style={{ flexDirection: "row", marginTop: verticalScale(15), marginLeft: scale(15), marginRight: scale(15), justifyContent: "space-between" }}>
                    <View style={{ width: scale(41), height: scale(41), borderRadius: 400, backgroundColor: this.props.bgcolor, justifyContent: "center", alignItems: "center", }} opacity={0.5}>
                        <Image
                            style={{ width: scale(23.27), height: scale(23.34) }}
                            source={this.props.image}
                            resizeMode='contain'
                        />
                    </View>
                    <Text style={{ color: this.props.textcolor, fontSize: scale(30), fontFamily: "Roboto-Bold" }}>{this.props.quantity}</Text>
                </View>

                <Text style={{ color: this.props.textcolor, fontSize: scale(10), fontFamily: "Roboto-Bold", marginLeft: scale(15), marginTop: verticalScale(10) }}>{this.props.card_name}</Text>

            </View>
        );
    }
}