import React, { Component } from 'react';
import { FlatList, Button, View, Text, TextInput, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

export default class PODropDown extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Modal
                isVisible={this.props.PODropDownisModalVisible}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                }}
                // animationInTiming={1000}
                // animationOutTiming={1000}
                // backdropTransitionInTiming={1000}
                // backdropTransitionOutTiming={1000}
                // backdropOpacity={0.8}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={600}
                animationOutTiming={600}
                backdropTransitionInTiming={600}
                backdropTransitionOutTiming={600}
            >
                <View style={{ width: scale(250), height: scale(305), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center", marginTop: verticalScale(10) }}>
                    <FlatList
                        data={this.props.podata}
                        extraData={this.state}
                        enableEmptySections={true}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity style={{ width: scale(200), height: scale(25), borderBottomWidth: 1, borderBottomColor: "#DFE6EA", marginTop: verticalScale(10), justifyContent: "center", }}
                                onPress={() => this.props.onPress(item)}>
                                <Text style={{ marginLeft: scale(8) }}>{item}</Text>
                            </TouchableOpacity>
                        )} />
                    <View style={{ height: scale(10) }} />
                </View>
            </Modal>
        );
    }
}