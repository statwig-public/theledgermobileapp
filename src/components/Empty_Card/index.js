import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    BackHandler,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class Empty_Card extends Component {
    constructor() {
        super();
        this.state = {

        };
    }

    render() {
        return (
            <View style={{ marginTop: verticalScale(50), backgroundColor: "#FFFFFF", borderRadius: 8, width: scale(328), alignItems: "center", justifyContent: "center", height: scale(421) }}>

<Text style={{ fontSize: scale(20), width: scale(190), textAlign: "center" }} numberOfLines={2}>Looks like your</Text>
                <Text style={{ fontSize: scale(20), width: scale(190), textAlign: "center" }} numberOfLines={2}>{this.props.Text}</Text>
                <Image style={{ width: scale(260), height: scale(211), marginTop: verticalScale(11) }}
                    resizeMode='cover'
                    source={require('../../assets/Empty.png')} />

            </View>
        );
    }
}