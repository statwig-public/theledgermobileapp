import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

export default class PopUp extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isModalVisible}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                }}
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}>
                <View style={{ width: scale(250), height: scale(300), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: scale(46.16), height: scale(46.16),}}
                        source={this.props.image}
                        resizeMode='contain'
                    />

                    <Text style={{ fontSize: scale(20), textAlign: 'center', marginTop:verticalScale(48),fontFamily:"Roboto-Bold" }}>{this.props.text}</Text>

                    <Text style={{ fontSize: scale(15), textAlign: 'center', marginTop:verticalScale(20),fontFamily:"Roboto-Regular",color:"#707070",marginRight:scale(27),marginLeft:scale(27) }}>{this.props.text2}</Text>

                    

                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                               
                                alignItems: 'center',
                                
                                borderRadius: 10,
                                width: scale(105),
                                marginTop:verticalScale(44),
                                
                                height: scale(35),
                            }}
                            onPress={this.props.Ok}>
                            <LinearGradient
                                start={{ x: 0.1, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                                locations={[0, 0.5, 0.6]}
                                colors={['#0093E9', '#36C2CF']}
                                style={{
                                    justifyContent: 'center',
                               
                                alignItems: 'center',
                                
                                borderRadius: 10,
                                width: scale(105),
                                
                                
                                height: scale(35),
                                }}>
                                <Text style={{ color: '#FFFFFF', fontFamily:"Roboto-Bold" , fontSize: scale(15) }}>{this.props.label}</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                       
                </View>
            </Modal>
        );
    }
}