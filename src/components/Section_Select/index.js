import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';


class Section_Select extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={{flexDirection:"row",marginTop:verticalScale(15),height:scale(35)}}>
                {
                    this.props.button.selected
                        ?
                        <View style={{marginLeft:this.props.button.label === "Shipments" ? 0 : scale(30)}}>
                            <Text style={{color:"#FA7923",fontSize:scale(14),fontFamily:"Roboto-Bold"}}>{this.props.button.label}</Text>
                            <View style={{width:scale(10),borderWidth:2,borderColor:"#FA7923",borderRadius:5}}/>
                        </View>
                        :
                        <View style={{marginLeft:this.props.button.label === "Shipments" ? 0 : scale(30)}}>
                            <Text style={{color:"#707070",fontSize:scale(14),fontFamily:"Roboto-Regular"}}>{this.props.button.label}</Text>
                        </View>
                }
            </TouchableOpacity>
        );
    }
}
export default Section_Select;