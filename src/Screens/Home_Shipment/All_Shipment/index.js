import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    RefreshControl,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    FlatList,
    ActivityIndicator,
    LayoutAnimation, UIManager,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Shipment_Card from "../../../components/Shipment_Card";
import Empty_Card from "../../../components/Empty_Card";
import { Total_Shipment, Total_Shipment_Sent, Total_Shipment_Received, Current_Shipment_InTransit, customiseShipmentData } from "../../../utils/Shipment_Count";
//testing
class All_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            fetching_from_server: false,
            isListEnd: false,
            skip:0
        }
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home_Shipment');
        return true;
    }
    // componentDidMount = async () => {
    //     const token = await AsyncStorage.getItem('token');
    //     console.log('async', token);
    //     console.log('redux', this.props.logindata);
    //     //  await this.props.userinfo()
    //     await this.props.userinfo()
    //     await this.props.inventory(0,5)
    //     //await this.props.fetch_shipmemnt()
    //     await this.props.fetchPublisherShipments()
    // }

    GetData = async () => {
        console.log("hi");
        // await this.props.userinfo()
        // await this.props.inventory(0,5)
        // await this.props.fetchPublisherShipments(0, 5)
        // await this.props.fetchAllUsers()
        // await this.props.fetch_Purchase_Orders_ids()
        // await this.props.fetch_Purchase_Orders()
    }
    onRefresh = async () => {
        //Call the Service to get the latest data
        this.GetData();
    }
    loadMoreData = () => {
        var newSkip = this.state.skip + 5;
        console.log("newskip",newSkip);
        // this.setState({skip:newSkip})
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            console.log("hello");
        this.setState({ fetching_from_server: true }, async () => { 
            const result = await this.props.fetchPublisherShipments(newSkip, 5)
            console.log("result",result);
            if(result.data.data.length > 0){
                this.setState({
                    skip:newSkip,
                    fetching_from_server: false,})
            }else{
            this.setState({
                fetching_from_server: false,
                isListEnd:true})
            }
        })
    }
        
        //console.log("result",result);
    }
    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.props.shipment];
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };

    renderFooter() {
        return (
          <View style={{padding: 10,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',}}>
            {this.state.fetching_from_server ? (
              <ActivityIndicator color="black" style={{ margin: 15 }} />
            ) : null}
          </View>
        );
      }


    render() {
        console.log('loder', this.props.loder);
        console.log('allll order', this.props.shipment);

        // if (this.props.loder) {
        //     return (
        //         <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        //             <ActivityIndicator />
        //         </View>
        //     );
        // }



        return (
            <View style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                //backgroundColor:"#000",

            }}>
                <View style={{ height: 5 }} />
                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    // refreshControl={
                    //     <RefreshControl
                    //         //refresh control used for the Pull to Refresh
                    //         onRefresh={this.onRefresh.bind(this)}
                    //         colors={['#a076e8', '#5dc4dd']}
                    //     />}
                    >
                    {/* {this.props.shipment.length === 0 ?
                    <View>
                    <Image style={{ marginTop: verticalScale(100),width: scale(100), height: scale(100),alignSelf:"center" }}
                        resizeMode='cover'
                        source={require('../../../assets/shipping.png')} />
                        <Text style={{fontSize:scale(20)}}>Shipments are Empty</Text>
                        </View>: 
                        this.props.shipment.map((item, key) => (
                            <>
                                
                                    <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} />
                                
                            </>
                        ))
                    } */}
                    {this.props.shipment.length === 0 ?
                        <View style={{ marginTop: verticalScale(-35) }}>
                            <Empty_Card Text="Shipments are Empty!" />
                        </View> :
                        // <Text>hi</Text>

                        <FlatList
                            style={{ width: '100%' }}
                            keyExtractor={(item, index) => index.toString()}
                            data={this.props.shipment}
                            onEndReached={()=>this.loadMoreData()}
                            onEndReachedThreshold={1}
                            renderItem={({ item, key }) => (
                                <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} />
                            )}
                            
                            ListFooterComponent={this.renderFooter()}
                        //Adding Load More button as footer component
                        />

                    //     this.props.shipment.map((item, key) => (
                    //         <>
                    //     {/* {item.shipmentId === "SHP1234" ? null : */}
                    //     <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} />

                    // </>
                    //     ))
                    }
                    <View style={{ height: scale(20) }} />
                </ScrollView>

            </View>
        )
    }
}

// const customiseShipmentData = (shipment) => {
//     let data = [];
//     for (let i in shipment) {
//         const Object = {};
//         const total = 0;
//         Object = shipment[i]
//         Object['isExpanded'] = false
//         data.push(Object);
//     }
//     console.log('data', data);
//     return data.reverse();
// }
function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata,
        logindata: state.login.logindata,
        user: state.userinfo.user,
        loder: state.loder,
        inventorydata: state.inventory.inventorydata,
        shipment: customiseShipmentData(state.shipment.shipmentdata, "All_Shipment"),
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders })(All_Shipment)
//export default All_Shipment


