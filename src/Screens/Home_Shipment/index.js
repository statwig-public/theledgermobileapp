import React from 'react';
import {
    LayoutAnimation, UIManager,
    Platform, Button, View, ScrollView, Text, 
    RefreshControl,
    StyleSheet, ImageBackground, TouchableOpacity, Image, StatusBar, Animated, ActivityIndicator
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../components/Header"
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Skeleton from "../../components/Skeleton"
import Shipment_Card from "../../components/Shipment_Card";
import Card from "../../components/Card";
import Empty_Card from "../../components/Empty_Card";
import Section_Select from "../../components/Section_Select";
import Purchase_Order_Card from "../../components/Purchase_Order_Card";
import { Total_Shipment, Total_Shipment_Sent, Total_Shipment_Received, Current_Shipment_InTransit, customiseShipmentData } from "../../utils/Shipment_Count";
import {customisepurchaseorderData} from  "../../utils/Purchase_Order_Data";

class Home_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            radioItems:
                [
                    {
                        label: 'Shipments',
                        selected: true
                    },

                    {
                        label: 'Purchase Order',
                        selected: false,
                    }
                ], selectedItem: ''

        }
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount = async () => {
        this.GetData();
    }

    GetData = async () => {
        const token = await AsyncStorage.getItem('token');
        console.log('async', token);
        console.log('redux', this.props.logindata);
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
        await this.props.userinfo()
        await this.props.inventory(0,5)
        await this.props.fetchPublisherShipments(0,5)
        await this.props.fetchAllUsers()
        await this.props.fetch_Purchase_Orders_ids()
        await this.props.fetch_Purchase_Orders()
    }

    onRefresh = async () => {
        //Call the Service to get the latest data
        this.GetData();
      }
    

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;

        console.log("hhhhh", index);

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }

    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.props.shipment];
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };

    purchase_order_click = (id) => {
        this.props.navigation.navigate('Purchase_Order', {id})
    }

    render() {
        //console.log("this.props.shipmentcounts.total",this.props.purchaseorder);

        if (this.props.loder) {
            return (
                <Skeleton />
            );
        }
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Shipment'} />

                <View style={{ width: scale(328), marginTop: verticalScale(-75) }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <Card image={require("../../assets/Total_Shipments.png")}
                            bgcolor="#35BFCC96"
                            textcolor="#2BA3AE"
                            // quantity={this.props.Total_Shipments}
                            quantity={this.props.shipmentcounts[0]}
                            card_name="Total Shipments" />

                        <Card image={require("../../assets/Total_Shipments_Received.png")}
                            bgcolor="#FFAB1D"
                            textcolor="#FA7923"
                            // quantity={this.props.Total_Shipment_Receiveds}
                            quantity={this.props.shipmentcounts[2]}
                            card_name="Total Shipments Received" />
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: verticalScale(10) }}>
                        <Card image={require("../../assets/Total_Shipments_Sent.png")}
                            bgcolor="#89D1F0"
                            textcolor="#0093E9"
                            // quantity={this.props.Total_Shipment_Sents}
                            quantity={this.props.shipmentcounts[1]}
                            card_name="Total Shipments Sent" />

                        <Card image={require("../../assets/Current_Shipment_InTransit.png")}
                            bgcolor="#0A69C5"
                            textcolor="#0159EA"
                            // quantity={this.props.Current_Shipment_InTransits}
                            quantity={this.props.shipmentcounts[3]}
                            card_name="Current Shipment InTransit" />
                    </View>

                </View>
                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} 
                    refreshControl={
                        <RefreshControl
                          //refresh control used for the Pull to Refresh
                          onRefresh={this.onRefresh.bind(this)}
                          colors={['#a076e8', '#5dc4dd']}
                        />
                      }
                    style={{ marginTop: verticalScale(20) }}>
                    <View style={{ width: scale(328), height: scale(55), backgroundColor: "transparent", marginTop: verticalScale(20), flexDirection: "row", justifyContent: "space-between", }}>
                        <TouchableOpacity style={{ width: scale(159), height: scale(55), borderRadius: 10, backgroundColor: "#FFAB1D", flexDirection: "row", }}
                            //disabled={true}
                            onPress={() => this.props.navigation.navigate('Shipment_Details')}>
                            <Image style={{ width: scale(13.73), height: scale(13.73), marginTop: verticalScale(15), marginLeft: scale(10) }}
                                resizeMode='center'
                                source={require('../../assets/add.png')} />
                            <View style={{ alignItems: "flex-start", justifyContent: "center", marginLeft: scale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>CREATE</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SHIPMENT</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: scale(159), height: scale(55), borderRadius: 10, backgroundColor: "#FA7923", flexDirection: "row", }}
                            //disabled={true}
                            onPress={() => this.props.navigation.navigate('Create_Purchase_Order')}>
                            <Image style={{ width: scale(13.14), height: scale(15), marginTop: verticalScale(15), marginLeft: scale(10) }}
                                resizeMode='center'
                                source={require('../../assets/Purchaseorder.png')} />
                            <View style={{ alignItems: "flex-start", justifyContent: "center", marginLeft: scale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>CREATE</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>PURCHASE ORDER</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ width: scale(328), height: scale(25), backgroundColor: "transparent", marginTop: verticalScale(20), flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Transactions</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Shipment')}>
                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", }}>SHOW MORE</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: "row", }}>
                        {
                            this.state.radioItems.map((item, key) =>
                                (
                                    <Section_Select key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                ))
                        }
                    </View>

                    {this.state.selectedItem !== "Shipments" ?
                        this.props.purchaseorder.length === 0 ?
                            <View style={{ marginTop: verticalScale(-35) }}>
                                <Empty_Card Text="Purchase Orders are Empty!" />
                            </View> :
                            this.props.purchaseorder.map((item, index) => (
                                <Purchase_Order_Card button={item} onClickFunction={()=>this.purchase_order_click(item.orderID)} />
                                )) :
                        this.props.shipment.length === 0 ?
                            <View style={{ marginTop: verticalScale(-35) }}>
                                <Empty_Card Text="Shipments are Empty!" />
                            </View> :
                            this.props.shipment.map((item, key) => (
                                <>
                                    {key < 5 ?
                                        <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} 
                                        onSendShipment={()=>{this.props.navigation.navigate('Send_Shipment',{data:item})}}/> : null}

                                </>
                            ))
                    }

                    <View style={{ height: scale(10) }} />
                </ScrollView>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    lottie: {
        width: scale(100),
        height: scale(100)
    }
})

// const customisepurchaseorderData = (purchaseorder, id) => {
//     let data = [];
//     for (let i in purchaseorder) {
//         const purchaseorderObject = {};
//         var sum = 0;
//         if (typeof purchaseorder[i].products == 'object') {
//             for (let key in purchaseorder[i].products) {
//                 Object.keys(purchaseorder[i].products[key]).forEach(element => {
//                     sum += parseFloat(purchaseorder[i].products[key][element])
//                 });
//             }
//             purchaseorderObject = purchaseorder[i]
//             purchaseorderObject['id'] = id[i]
//             purchaseorderObject['total'] = sum
//             data.push(purchaseorderObject);
//         }
//     }

//     return data.reverse();
// }


function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata,
        logindata: state.login.logindata,
        user: state.userinfo.user,
        loder: state.loder,
        inventorydata: state.inventory.inventorydata,
        shipmentcounts: state.shipment.shipmentcounts,
        shipment: customiseShipmentData(state.shipment.shipmentdata, "HomeShipment"),
        Total_Shipments: Total_Shipment(state.shipment.shipmentdata),
        Total_Shipment_Sents: Total_Shipment_Sent(state.shipment.shipmentdata),
        Current_Shipment_InTransits: Current_Shipment_InTransit(state.shipment.shipmentdata),
        Total_Shipment_Receiveds: Total_Shipment_Received(state.shipment.shipmentdata),
        purchaseorder: customisepurchaseorderData(state.purchaseorder.purchaseorder, 
            //state.purchaseorder.purchaseorderids, 
            "HomeShipment"),
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders })(Home_Shipment)