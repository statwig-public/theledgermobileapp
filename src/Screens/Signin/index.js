import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Keyboard,
    Switch,
    Alert,
    BackHandler,
    StatusBar
} from 'react-native';
import { connect } from 'react-redux';
import { login, } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import FloatingLabelInput from "../../components/FloatingLabelInput";
import LinearGradient from 'react-native-linear-gradient';
import setAuthToken from "../../utils/setAuthToken"
import PopUp from "../../components/PopUp";

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
}

class Signin extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            name: '',
            pass: '',
            hidePassword: true,
            remember_me: false,
            error_email: null,
            error_password: null,
            error_email_line: false,
            error_pwd_line: false,
            isModalVisible: false,
            isModalVisible2: false
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        BackHandler.exitApp();
        return true;
    }

    componentDidMount() {
        this.requestCameraPermission();
    }

    requestCameraPermission = async () => {
        try {
            const grantedCamera = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
            );
            const grantedStorage = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            );
            if (grantedStorage === PermissionsAndroid.RESULTS.GRANTED || grantedCamera === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera");
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };

    _signin = async () => {
        var {
            name,
            pass,
        } = this.state
        const data = {
            "email": name,
            "password": pass
        }
        if (name == "" && pass == "") this.setState({ error_email: "Enter your Email", error_password: "Enter your Password", error_email_line: true, error_pwd_line: true })
        else if (name == "") this.setState({ error_email: "Enter your Email", error_email_line: true, error_password: null, error_pwd_line: false });
        else if (pass == "") this.setState({ error_password: "Enter your Password", error_pwd_line: true, error_email: null, error_email_line: false });
        else {
            const result = await this.props.login(data, this.state.remember_me)
            console.log('re', result.status);

            if (result.status === 200) {
                // Set auth token auth
                console.log('log');
                this.setState({
                    isModalVisible: true
                })
                const token = result.data.data.token;
                setAuthToken(token);
            } else if (result.data.message === "Email or Password wrong.") {

                this.setState({ error_email: "Incorrect Email", error_password: "Incorrect Password", error_email_line: true, error_pwd_line: true })
            } else {
                this.setState({
                    isModalVisible2: true
                })
            }
            //this.props.login(data, this.state.remember_me, () => this.props.navigation.navigate('App'));
        }
    }

    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('App')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }

    _remember_me = () => {
        if (this.state.remember_me == false) {
            this.setState({ remember_me: true });
            //   this.props.remeber_me(this.state.remember_me)
        } else {
            this.setState({ remember_me: false });
            //   this.props.remeber_me(this.state.remember_me)
        }
    };

    setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: "transparent",
                justifyContent: "center"
            }}>
                {/* <KeyboardAvoidingView
                style={styles.wrapper}
                behavior="height" enabled> */}
                <StatusBar hidden />
                <ImageBackground style={styles.imgBackground}
                    resizeMode='stretch'
                    source={require('../../assets/Background.png')}>
                    <View style={{
                        flex: 1,
                        backgroundColor: "transparent",
                        justifyContent: "center"
                    }}>
                        <PopUp isModalVisible={this.state.isModalVisible}
                            image={require('../../assets/Sucess.png')}
                            text="Success!"
                            text2="Login Successfully !"
                            label="Ok"
                            Ok={() => this.ok()} />
                        <PopUp isModalVisible={this.state.isModalVisible2}
                            image={require('../../assets/fail.png')}
                            text="Fail!"
                            text2="Something went wrong. Please try again."
                            label="Try again"
                            Ok={() => this.fail()} />

                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}>
                            <Image style={{ marginTop: verticalScale(79), marginLeft: scale(30), width: scale(198), height: scale(24) }}
                                resizeMode='cover'
                                source={require('../../assets/VACCINELEDGER.png')} />
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(8), fontSize: scale(26), color: '#000000', fontFamily: "Roboto-Regular" }}>
                                Welcome Back ,
                        </Text>
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(9), fontSize: scale(13), color: '#000000', fontFamily: "Roboto-Light" }}>
                                Login to continue
                        </Text>

                            <View style={[{ marginLeft: scale(30), marginRight: scale(30), height: scale(380), borderRadius: 10, backgroundColor: "#FFFFFF", marginTop: verticalScale(40), justifyContent: "center", alignItems: "center" }, shadowStyle]}>
                                <Text style={{ marginTop: verticalScale(20), fontSize: scale(20), color: '#000000', fontFamily: "Roboto-Bold", fontWeight: "bold" }}>
                                    Login
                        </Text>

                                <View style={{ width: "100%" }}>
                                    <FloatingLabelInput
                                        label="Email"
                                        onChangeText={(name) => this.setState({ name })}
                                        value={this.state.name}
                                        error={this.state.error_email_line}
                                        image={require('../../assets/mail.png')}
                                    />
                                    <Text style={{ marginLeft: scale(34), color: "#D80000", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", }}>{this.state.error_email}</Text>
                                </View>

                                <View style={{ width: "100%", marginTop: verticalScale(15) }}>
                                    <FloatingLabelInput
                                        label="Password"
                                        onChangeText={(pass) => this.setState({ pass })}
                                        value={this.state.pass}
                                        secureTextEntry={this.state.hidePassword}
                                        image={require('../../assets/key.png')}
                                        password={true}
                                        error={this.state.error_pwd_line}
                                        passimage={(this.state.hidePassword) ? require('../../assets/HidePassword.png') : require('../../assets/ShowPassword.png')}
                                        onClick={this.setPasswordVisibility}
                                    />
                                    <Text style={{ marginLeft: scale(34), color: "#D80000", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", }}>{this.state.error_password}</Text>
                                </View>

                                <View style={{ marginLeft: scale(34), marginRight: scale(34), width: scale(218), alignItems: "flex-end", }}>
                                    <TouchableOpacity>
                                        <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#A8A8A8", marginTop: verticalScale(7) }}>
                                            Forgot Password ?
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                                {/* <Text style={{ color: "red", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", alignSelf: "center" }}>{this.state.error}</Text> */}

                                <View style={{ marginTop: verticalScale(5), flexDirection: "row", width: scale(218), }}>
                                    <TouchableOpacity style={{ width: scale(15), height: scale(15), borderRadius: 4, borderWidth: !this.state.remember_me ? 1 : 0, borderColor: "#707070", alignItems: "center", justifyContent: "center" }}
                                        onPress={() => this._remember_me()}>
                                        {!this.state.remember_me ? null :
                                            <Image style={{ width: scale(13), height: scale(13), }}
                                                source={require('../../assets/check.png')}
                                                resizeMode="cover" />}
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(9) }}>Remember me</Text>


                                </View>

                                <TouchableOpacity
                                    onPress={this._signin}
                                    style={{ width: scale(133), height: scale(40), borderRadius: 8, backgroundColor: "red", justifyContent: "center", alignItems: "center", marginTop: verticalScale(15) }}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                        colors={['#0093E9', '#36C2CF']}
                                        style={{ width: scale(133), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ fontSize: scale(20), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>LOGIN</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", marginTop: verticalScale(17) }}>
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#707070" }}>
                                        Don’t have an account?
                </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
                                        <Text style={{ marginLeft: scale(7), fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#0B65C1", fontWeight: "bold" }}>
                                            Sign Up
                </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ height: scale(10) }} />
                        </ScrollView>
                    </View>
                </ImageBackground>
                {/* </KeyboardAvoidingView> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
        position: 'absolute',
    },
    wrapper: {
        flex: 1,
    },
})

function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata,
    }
}

export default connect(mapStateToProps, { login, })(Signin)