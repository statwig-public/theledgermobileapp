import React from 'react';
import { Button, View, Text, StyleSheet, ScrollView, BackHandler, ImageBackground, Image, TouchableOpacity, StatusBar, Animated } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../../components/Scale';
import { connect } from 'react-redux';
import { add_inventory,inventory } from '../../../../Action';
import Header from "../../../../components/Header"
import PopUp from "../../../../components/PopUp";

class Review_Inventory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.navigation.getParam('data'),
            isModalVisible: false,
            isModalVisible2: false,
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Add_Inventory');
        return true;
    }
    componentDidMount = async () => {
        console.log("Create_Purchase_Order", this.state.data);
    }

    _SaveInventory = async () => {
        console.log('save', this.state.data);
        console.log('bbbbbb', this.props.inventorydata);

        // this.props.navigation.navigate('Review_Inventory',{
        //     data: this.state.data,})

        //await this.props.add_inventory(this.state.data,() => this.props.navigation.navigate('Inventory'))

    //    for(var i = 0 ; i <= this.state.data.length ; i++){
    //        if(this.state.data[i].productName && this.state.data[i].manufacturerName && this.state.data[i].quantity &&this.state.data[i].manufacturingDate &&this.state.data[i].expiryDate &&this.state.data[i].batchNumber &&this.state.data[i].serialNumber &&this.state.data[i].storageConditionmax &&this.state.data[i].storageConditionmin){
    //            console.log('hi',i);
               
    //        }else{
    //            console.log("errr",i);
               
    //         //alert('Please fill all the details to Proceed')
    //        }
           
    //    }
       
        const responses = await this.props.add_inventory(this.state.data)
        for (let index in responses) {
            let response = responses[index]
            if (response.status !== 200) {
                //if one status is failed so it is going to the catch
                //alert('Failed to Add Inventory')
                this.setState({ isModalVisible2: true })
                throw new Error('inventory unsuccessful')
            }
        }
        const res = await this.props.inventory(0,5)
        if(res.status === 200){
            this.setState({
                isModalVisible: true
            })
        }
        
    }

    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('Inventory')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }

    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Add Inventory'} />

                <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", marginTop: verticalScale(-75), alignSelf: "flex-start", marginLeft: scale(16), color: "#FFFFFF" }}>
                    Review Inventory
                </Text>
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(14) }}>
                    <PopUp isModalVisible={this.state.isModalVisible}
                        image={require('../../../../assets/Sucess.png')}
                        text="Success!"
                        text2="Your Inventory has been added successfully!"
                        label="Ok"
                        Ok={() => this.ok()} />
                    <PopUp isModalVisible={this.state.isModalVisible2}
                        image={require('../../../../assets/fail.png')}
                        text="Fail!"
                        text2="Something went wrong. Please try again."
                        label="Try again"
                        Ok={() => this.fail()} />

                    {this.state.data.map((item, index) => (
                        <View style={{ width: scale(328), height: scale(240), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(14) }}>
                            <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                                <View style={{ flexDirection: "row", alignItems: "center", }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Product Name</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.productName}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Manufacturer</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.manufacturerName}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Quantity</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.quantity}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Mfg Date</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.manufacturingDate}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Exp Date</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.expiryDate}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Storage Conditions</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.storageConditionmin} - {item.storageConditionmax}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Batch Number</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.batchNumber}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Serial Numbers</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.serialNumber}</Text>
                                </View>
                            </View>

                        </View>
                    ))}
                    {/* <View style={{ height: scale(15) }} /> */}
                </ScrollView>
                <View style={{ height: scale(60) }} />
                <View style={{ flexDirection: "row", width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(20), position: 'absolute', bottom: 10, justifyContent: "space-between" }}>
                    {/* position: 'absolute', bottom: 10, */}
                    <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}
                        //disabled={true}
                        onPress={() => this.props.navigation.navigate('Add_Inventory')}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../../assets/edit.png')} />
                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold", marginLeft: scale(10) }}>EDIT</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this._SaveInventory()}
                        style={{ width: scale(195), height: scale(40), borderRadius: 8, flexDirection: "row", backgroundColor: "#FFAB1D", alignItems: "center", justifyContent: "space-evenly" }}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../../assets/add.png')} />
                        <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>ADD INVENTORY</Text>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})

function mapStateToProps(state) {
    return {
        user: state.userinfo.user,
        inventorydata: state.inventory.inventorydata,
    }
}

export default connect(mapStateToProps, { add_inventory,inventory })(Review_Inventory)

//export default Review_Inventory;