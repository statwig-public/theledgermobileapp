import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    StatusBar,
    // Picker,
    ImageBackground,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    BackHandler,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { add_inventory } from '../../../Action';
import Picker from 'react-native-picker';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import HamburgerIcon from '../../../components/HamburgerIcon';
import Inventory from '..';
import PopUp from "../../../components/PopUp";
import { CameraKitCameraScreen, } from 'react-native-camera-kit';

class Add_Inventory extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            qrvalue: '',
            opneScanner: false,

            isModalVisible: false,
            isModalVisible2: false,
            data: [{
                productName: "",
                manufacturerName: "",
                quantity: "",
                manufacturingDate: "",
                expiryDate: "",
                //storageCondition: "",
                batchNumber: "",
                serialNumber: "",
                storageConditionmax: "",
                storageConditionmin: ""

            }]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Inventory');
        return true;
    }

    _createDateData() {
        let date = [];
        for (let i = 2016; i < 2050; i++) {
            let month = [];
            for (let j = 1; j < 13; j++) {
                let _month = {};
                _month[j] = j;
                month.push(j);
            }
            let _date = {};
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }

    _createCelsiusData() {
        let Celsius = [];
        for (let i = -30; i < 31; i++) {
            let left = [];
            for (let j = -30; j < 31; j++) {
                let _left = {};
                _left[j] = j;
                left.push(j + '°C');
            }
            let _right = {};
            _right[i + '°C'] = left;
            Celsius.push(_right);
        }
        return Celsius;
    }

    _showCelsiusPicker(index) {
        Picker.init({
            pickerData: this._createCelsiusData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Storage Conditions",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].storageConditionmin = pickedValue[0]
                productClone[index].storageConditionmax = pickedValue[1]
                this.setState({ data: productClone })
                // var date = pickedValue[0] + "  -  " + pickedValue[1]
                // const productClone = JSON.parse(JSON.stringify(this.state.data));
                // productClone[index].storageCondition = date
                // this.setState({data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                // var date = pickedValue[0] + "  -  " + pickedValue[1]
                // const productClone = JSON.parse(JSON.stringify(this.state.data));
                // productClone[index].storageCondition = date
                // this.setState({data: productClone })
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].storageConditionmin = pickedValue[0]
                productClone[index].storageConditionmax = pickedValue[1]
                this.setState({ data: productClone })
            }
        });
        Picker.show();
    }

    _showMfgDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturing Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturingDate = date
                this.setState({ data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturingDate = date
                this.setState({ data: productClone })
            }
        });
        Picker.show();
    }

    _showExpDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Expiry Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                if (this.state.data[index].manufacturingDate) {
                    const manufacturingDate = this.state.data[index].manufacturingDate.split('/')[1] + "/" + this.state.data[index].manufacturingDate.split('/')[0]
                    var date2 = pickedValue[0] + "/" + pickedValue[1]
                    var date2 = pickedValue[0] + "/" + pickedValue[1]
                    var val = date2 > manufacturingDate
                    var d = new Date();
                    var month = d.getMonth() + 1;
                    var year = d.getFullYear();
                    var current = year + "/" + month
                    var check = current < date2
                    // console.log("my",month,year,current,current < date2,date2);
                    // console.log("date",date2,"manufacturingDate",manufacturingDate,pickedValue[0]);
                    // console.log("expiryDate", val);
                    //console.log("expiryDate", val, pickedValue[0] + "/" + pickedValue[1], new Date(date2) > new Date(Date.parse(manufacturingDate)));
                    if (!val || !check) {
                        alert('Invalid Expiry Date')
                        
                    } else {
                        var date = pickedValue[1] + "/" + pickedValue[0]
                        const productClone = JSON.parse(JSON.stringify(this.state.data));
                        productClone[index].expiryDate = date
                        this.setState({ data: productClone })
                    }
                }
                else {
                    alert("First Select Manufacturing Date")
                }
                // var date = pickedValue[1] + "/" + pickedValue[0]
                // const productClone = JSON.parse(JSON.stringify(this.state.data));
                // productClone[index].expiryDate = date
                // this.setState({ data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                console.log("pickedValue", pickedValue);
                // var date = pickedValue[1] + "/" + pickedValue[0]
                // const productClone = JSON.parse(JSON.stringify(this.state.data));
                // productClone[index].expiryDate = date
                // this.setState({ data: productClone })
            }
        });
        Picker.show();
    }

    _showProductName(index) {
        Picker.init({
            pickerData: ['bOPV', 'MMR', 'PVC', 'BCG', 'RV', 'Hep B'],
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Product",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].productName = value
                this.setState({ data: productClone })

            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].productName = value
                this.setState({ data: productClone })
            }
        });
        Picker.show();
    }

    _showManufacturer(index) {
        Picker.init({
            pickerData: ['Bharat Biotech', 'Aroma Biotech', 'Chronly industries', 'GH Pharmas',],
            pickerTextEllipsisLen: 30,
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturer",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturerName = value
                this.setState({ data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturerName = value
                this.setState({ data: productClone })

            }
        });
        Picker.show();
    }

    _add_another_inventory() {
        this.setState({
            data: [...this.state.data, {
                productName: '',
                manufacturerName: '',
                quantity: '',
                manufacturingDate: "",
                expiryDate: "",
                //storageCondition: "",
                batchNumber: "",
                serialNumber: "",
                storageConditionmax: "",
                storageConditionmin: ""
            }]
        })
    }

    updateValue(index, item) {
        const name = Object.keys(item);
        if (name[0] === "batchNumber") {
            if (!/[^a-zA-Z0-9-]/.test(item.batchNumber)) {
                this.setState({
                    data: [
                        ...this.state.data.slice(0, index),
                        Object.assign({}, this.state.data[index], item),
                        ...this.state.data.slice(index + 1)
                    ]
                });
            } else {
                alert('Batch Number Does not Accept Special Characters')
            }
        } else if (name[0] === "serialNumber") {
            if (!/[^a-zA-Z0-9-]/.test(item.serialNumber)) {
                this.setState({
                    data: [
                        ...this.state.data.slice(0, index),
                        Object.assign({}, this.state.data[index], item),
                        ...this.state.data.slice(index + 1)
                    ]
                });
            } else {
                alert('Serial Number Does not Accept Special Characters')
            }
        } else if (name[0] === "quantity") {
            const re = /^[0-9\b]+$/;
            // if (parseInt(item.quantity) > 0) {
            // if(/^\d+$/.test(parseInt(item.quantity)) || parseInt(item.quantity) > 0){
            // if(item.quantity.replace(/\D/gm,"").length > 0){
            if (parseInt(item.quantity) > 0 && re.test(item.quantity)) {
                this.setState({
                    data: [
                        ...this.state.data.slice(0, index),
                        Object.assign({}, this.state.data[index], item),
                        ...this.state.data.slice(index + 1)
                    ]
                });
            } else {
                alert('Invalid Quantity')
            }
        }

        console.log('data', this.state.data);
    }

    _SaveInventory = async () => {
        console.log('save', this.state.data);
        console.log('bbbbbb', this.props.inventorydata);

        // this.props.navigation.navigate('Review_Inventory',{
        //     data: this.state.data,})

        //await this.props.add_inventory(this.state.data,() => this.props.navigation.navigate('Inventory'))

        //    for(var i = 0 ; i <= this.state.data.length ; i++){
        //        if(this.state.data[i].productName && this.state.data[i].manufacturerName && this.state.data[i].quantity &&this.state.data[i].manufacturingDate &&this.state.data[i].expiryDate &&this.state.data[i].batchNumber &&this.state.data[i].serialNumber &&this.state.data[i].storageConditionmax &&this.state.data[i].storageConditionmin){
        //            console.log('hi',i);
        //            check.push("true")
        //        }else{
        //            console.log("errr",i);
        //            check.push("false")
        //         //alert('Please fill all the details to Proceed')
        //        }
        //        //console.log("hello"); 
        //    }


        for (let index in this.state.data) {
            let res = this.state.data[index]
            if (!res.productName || !res.manufacturerName || !res.quantity || !res.manufacturingDate || !res.expiryDate || !res.batchNumber || !res.serialNumber || !res.storageConditionmax || !res.storageConditionmin) {
                alert('Please fill all the details to Proceed')
                throw new Error('inventory unsuccessful')
            }
        }

        this.props.navigation.navigate('Review_Inventory', {
            data: this.state.data,
        })


        // const responses = await this.props.add_inventory(this.state.data)
        // for (let index in responses) {
        //     let response = responses[index]
        //     if (response.status !== 200) {
        //         //if one status is failed so it is going to the catch
        //         //alert('Failed to Add Inventory')
        //         this.setState({ isModalVisible2: true })
        //         throw new Error('inventory unsuccessful')
        //     }
        // }
        // this.setState({
        //     isModalVisible: true
        // })
    }
    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('Inventory')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }
    cancel = (index) => {
        console.log("hello", index);
        const productClone = JSON.parse(JSON.stringify(this.state.data));
        console.log(Object.keys(productClone));
        productClone.splice(index, 1)
        this.setState({ data: productClone })
    }
    AsyncAlert(message) {
        return new Promise((resolve, reject) => {
            Alert.alert(
                '',
                message,
                [
                    { text: 'OK', onPress: () => resolve('YES') },
                ],
                { cancelable: false }
            )
        })
    }
    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    onBarcodeScan(qrvalue) {
        //called after te successful scanning of QRCode/Barcode
        const productClone = JSON.parse(JSON.stringify(this.state.data));
        const index = this.state.index;

        if (!this.isJson(qrvalue)) {
            this.AsyncAlert("invalid QR Code").then(() => {
                this.setState({ opneScanner: true })
            })
            return
        }

        const value = JSON.parse(qrvalue);
        console.log('ggg', value);
        productClone[index].manufacturingDate = value.mfg
        productClone[index].expiryDate = value.exp
        productClone[index].storageConditionmax = value.s_max
        productClone[index].storageConditionmin = value.s_min
        productClone[index].batchNumber = value.batchno
        productClone[index].serialNumber = value.serialno

        this.setState({ data: productClone })
        this.setState({ qrvalue: qrvalue });
        this.setState({ opneScanner: false });
    }

    onOpneScanner(i) {
        var that = this;
        //To Start Scanning
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'CameraExample App Camera Permission',
                        'message': 'CameraExample App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //If CAMERA Permission is granted
                        console.log("start");
                        that.setState({ index: i });
                        that.setState({ qrvalue: '' });
                        that.setState({ opneScanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            //Calling the camera permission function
            requestCameraPermission();
        } else {
            console.log("start");

            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
        }
    }

    render() {
        // console.log('render', this.props.inventorydata);
        var Inventory_Details = [];
        for (let i = 0; i < this.state.data.length; i++) {
            Inventory_Details.push(
                <View style={{ marginTop: verticalScale(20), width: scale(328), height: scale(650), backgroundColor: "#FFFFFF", borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                    <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>

                        <View style={{ flexDirection: "row", height: scale(35), marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Product Name</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showProductName(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].productName === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].productName === "" ? "Select Product" : this.state.data[i].productName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Manufacturer</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showManufacturer(i)}
                            >
                                <Text style={{ width: "85%", color: this.state.data[i].manufacturerName === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].manufacturerName === "" ? "Select Manufacturer" : this.state.data[i].manufacturerName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Quantity</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Quantity"
                                    keyboardType="number-pad"
                                    value={this.state.data[i].quantity}
                                    onChangeText={(quantity) => this.updateValue(i, { quantity })} />
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Mfg Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showMfgDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].manufacturingDate === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].manufacturingDate === "" ? "MM/YYYY" : this.state.data[i].manufacturingDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>


                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Exp Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showExpDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].expiryDate === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].expiryDate === "" ? "MM/YYYY" : this.state.data[i].expiryDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Storage Conditions</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showCelsiusPicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].storageConditionmin === "" || this.state.data[i].storageConditionmax === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].storageConditionmin === "" && this.state.data[i].storageConditionmax === "" ? "Enter Storage Conditions" : this.state.data[i].storageConditionmin + " to " + this.state.data[i].storageConditionmax}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Batch Number</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Batch Number"
                                    value={this.state.data[i].batchNumber}
                                    onChangeText={(batchNumber) => this.updateValue(i, { batchNumber })} />
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Serial Numbers</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Serial Numbers"
                                    value={this.state.data[i].serialNumber}
                                    onChangeText={(serialNumber) => this.updateValue(i, { serialNumber })} />
                            </View>

                        </View>


                        <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
                            <TouchableOpacity
                                style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), marginRight: scale(20), borderRadius: 8, borderWidth: 1, borderColor: this.state.data.length === 1 ? "#D6D6D6" : "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}
                                onPress={() => this.cancel(i)}
                                disabled={this.state.data.length === 1 ? true : false}
                            >
                                <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: this.state.data.length === 1 ? "#D6D6D6" : "#0093E9", fontWeight: "bold" }}>CANCEL</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}
                                onPress={() => this.onOpneScanner(i)}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={['#0093E9', '#36C2CF']}
                                    style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}>
                                    <Image style={{ width: scale(15), height: scale(15) }}
                                        resizeMode='center'
                                        source={require('../../../assets/Scan.png')} />
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SCAN</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>)
        }
        if (!this.state.opneScanner) {
            return (
                <View style={{
                    flex: 1,
                    alignItems: "center",
                }}>
                    <StatusBar backgroundColor="#0093E9" />
                    <Header name={'Add Inventory'} />
                    <ScrollView nestedScrollEnabled={true}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-71) }}>
                        <PopUp isModalVisible={this.state.isModalVisible}
                            image={require('../../../assets/Sucess.png')}
                            text="Success!"
                            text2="Your Inventory has been added successfully!"
                            label="Ok"
                            Ok={() => this.ok()} />
                        <PopUp isModalVisible={this.state.isModalVisible2}
                            image={require('../../../assets/fail.png')}
                            text="Fail!"
                            text2="Something went wrong. Please try again."
                            label="Try again"
                            Ok={() => this.fail()} />

                        {Inventory_Details}


                        <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(58), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                            onPress={() => this._add_another_inventory()}>
                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../../assets/plus.png')} />
                            <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Add Another Product</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(40), backgroundColor: "#FFAB1D", borderRadius: 10, alignItems: "center", justifyContent: "center" }}
                            onPress={() => this._SaveInventory()}>
                            {/* <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../assets/add.png')} /> */}
                            <Text style={{ marginLeft: scale(13), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>REVIEW</Text>
                        </TouchableOpacity>

                        <View style={{ height: scale(25) }} />
                    </ScrollView>
                </View >
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <CameraKitCameraScreen
                    showFrame={true}
                    //Show/hide scan frame
                    scanBarcode={true}
                    //Can restrict for the QR Code only
                    laserColor={'green'}
                    //Color can be of your choice
                    frameColor={'green'}
                    //If frame is visible then frame color
                    colorForScannerFrame={'black'}
                    //Scanner Frame color
                    onReadCode={event =>
                        this.onBarcodeScan(event.nativeEvent.codeStringValue)
                    }
                />
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.userinfo.user,
        inventorydata: state.inventory.inventorydata,
    }
}

export default connect(mapStateToProps, { add_inventory })(Add_Inventory)