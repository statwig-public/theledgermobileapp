import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    StatusBar,
    ImageBackground,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    BackHandler,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { Count } from "../../../utils/Shipment_Count";
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import HamburgerIcon from '../../../components/HamburgerIcon';

class Inventory_Show_More extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [{
            vaccine: "BCG",
            fullname: "Bacille Calmette Guerin Vaccine",
            qty: "20,000",
            color: "#D9E5EF",
        },
        {
            vaccine: "OPV",
            fullname: "Oral Polio Vaccine",
            qty: "20,000",
            color: "#FFBCC4",
        },
        {
            vaccine: "HepB",
            fullname: "Hepatitis B Vaccine",
            qty: "20,000",
            color: "#FFEF83",
        },
        {
            vaccine: "DTwP",
            fullname: "Diphtheria and tetanus toxoids & whole-cell pertussis Vaccine",
            qty: "20,000",
            color: "#FFD0CA",
        },
        {
            vaccine: "IPV",
            fullname: "Inactivated Polio Vaccine",
            qty: "20,000",
            color: "#EBDDED",
        },
        {
            vaccine: "HiB",
            fullname: "Haemophilus influenzae Type B Vaccine",
            qty: "20,000",
            color: "#F1DDC6",
        },
        {
            vaccine: "RV",
            fullname: "Rotavirus Vaccine",
            qty: "20,000",
            color: "#D4EEEC",
        },
        {
            vaccine: "PVC",
            fullname: "Pneumococcal conjugate Vaccine",
            qty: "20,000",
            color: "#FFC18C",
        },
        {
            vaccine: "MMR",
            fullname: "Mumps, Measles & Rubella Vaccine",
            qty: "20,000",
            color: "#C1E3F2",
        },];
        this.state = {
            text: '',
            data:
                [
                    {
                        vaccine: "BCG",
                        fullname: "Bacille Calmette Guerin Vaccine",
                        qty: "20,000",
                        color: "#D9E5EF",
                    },
                    {
                        vaccine: "OPV",
                        fullname: "Oral Polio Vaccine",
                        qty: "20,000",
                        color: "#FFBCC4",
                    },
                    {
                        vaccine: "HepB",
                        fullname: "Hepatitis B Vaccine",
                        qty: "20,000",
                        color: "#FFEF83",
                    },
                    {
                        vaccine: "DTwP",
                        fullname: "Diphtheria and tetanus toxoids & whole-cell pertussis Vaccine",
                        qty: "20,000",
                        color: "#FFD0CA",
                    },
                    {
                        vaccine: "IPV",
                        fullname: "Inactivated Polio Vaccine",
                        qty: "20,000",
                        color: "#EBDDED",
                    },
                    {
                        vaccine: "HiB",
                        fullname: "Haemophilus influenzae Type B Vaccine",
                        qty: "20,000",
                        color: "#F1DDC6",
                    },
                    {
                        vaccine: "RV",
                        fullname: "Rotavirus Vaccine",
                        qty: "20,000",
                        color: "#D4EEEC",
                    },
                    {
                        vaccine: "PVC",
                        fullname: "Pneumococcal conjugate Vaccine",
                        qty: "20,000",
                        color: "#FFC18C",
                    },
                    {
                        vaccine: "MMR",
                        fullname: "Mumps, Measles & Rubella Vaccine",
                        qty: "20,000",
                        color: "#C1E3F2",
                    },


                ],
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Inventory');
        return true;
    }

    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.vaccine ? item.vaccine.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            data: newData,
            text: text,
        });
    }
    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Inventory'} />
                {/* <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={['#0093E9', '#36C2CF']}
                    style={{ width: Dimensions.get('window').width, height: scale(180), borderBottomLeftRadius: 24, borderBottomRightRadius: 24 }}>
                    <View style={{ marginTop: verticalScale(50), marginLeft: scale(16), flexDirection: "row", alignItems: "center" }}>
                        <HamburgerIcon />
                        <Text style={{ marginLeft: scale(36), fontSize: scale(24), fontFamily: "Roboto-Bold", color: "#FFFFFF",width:"75%" }}>Inventory</Text>
                        <Image
                            style={{ width: scale(13.5), height: scale(15),alignSelf:"center"}}
                            source={require("../../../assets/Bell.png")}
                            resizeMode='contain'
                        />
                    </View>
                </LinearGradient> */}

                <View style={{ width: scale(328), height: scale(32), backgroundColor: "#FFFFFF", borderRadius: 8, flexDirection: "row", marginTop: verticalScale(-71), justifyContent: "center", alignItems: "center" }}>
                    <TextInput style={{ width: "80%", }}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        value={this.state.text} />
                    <Image style={{ width: scale(16.85), height: scale(16.85), marginLeft: scale(15), }}
                        resizeMode='center'
                        source={require('../../../assets/Search.png')} />
                </View>

                <View style={{ width: scale(328), height: scale(69), backgroundColor: "#FFFFFF", borderRadius: 8, flexDirection: "row", marginTop: verticalScale(18), }}>

                    <Image style={{ width: scale(22.15), height: scale(22.15), marginLeft: scale(20), marginTop: verticalScale(18) }}
                        resizeMode='center'
                        source={require('../../../assets/Inventory1.png')} />

                    <View style={{ marginLeft: scale(16.85), marginTop: verticalScale(18), width: "50%" }}>
                        <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Total Inventory</Text>
                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", marginTop: scale(7) }}>Today</Text>
                    </View>

                    <View style={{ marginTop: verticalScale(18), }}>
                        <Text style={{ fontSize: scale(19), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{this.props.Counts}</Text>
                    </View>
                </View>

                <View style={{ width: Dimensions.get('window').width, flex: 1, }}>
                    <View style={{ flexDirection: "row" }}>
                        {/* <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF" }}>
                        <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#D9E5EF", justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>BCG</Text>
                        </View>
                        <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), width: scale(74), }} numberOfLines={1}>Bacille Calmette Guerin Vaccine</Text>
                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(24), }}>Qty: 20,000</Text> 
                 </View> */}

                        <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                            <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#FFBCC4", justifyContent: "center", alignItems: "center" }}>

                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>bOPV</Text>
                            </View>
                            <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Oral Polio Vaccine</Text>

                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.bOPV}</Text>
                        </View>
                        <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                            <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#D9E5EF", justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>BCG</Text>
                            </View>
                            <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Bacille Calmette Guerin Vaccine</Text>

                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.BCG}</Text>
                        </View>
                        <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                            <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#FFC18C", justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>PVC</Text>
                            </View>
                            <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Pneumococcal conjugate Vaccine</Text>

                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.PVC}</Text>
                        </View>

                    </View>
                    <View style={{flexDirection:"row"}}>
                    <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                                        <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#C1E3F2", justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>MMR</Text>
                                        </View>
                                        <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Mumps, Measles & Rubella Vaccine</Text>

                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.MMR}</Text>
                                    </View>

                                    <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                                        <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#D4EEEC", justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>RV</Text>
                                        </View>
                                        <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Rotavirus Vaccine</Text>

                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.RV}</Text>
                                    </View>

                                    <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                                        <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: "#FFEF83", justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>HepB</Text>
                                        </View>
                                        <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>Hepatitis B Vaccine</Text>

                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {this.props.vaccinecounts.HepB}</Text>
                                    </View>

                    </View>

                    {/* <FlatList
                        data={this.state.data}
                        extraData={this.state}
                        enableEmptySections={true}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        keyExtractor={(item, index) => index.toString()}
                        numColumns={3}
                        renderItem={({ item, index }) => (

                            <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15),borderRadius: 8  }}>
                                <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: item.color, justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{item.vaccine}</Text>
                                </View>
                                <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5),marginBottom:verticalScale(20), width: scale(74),textAlign:"center" }} numberOfLines={2}>{item.fullname}</Text>

                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7) ,position: 'absolute',}}>Qty: {item.qty}</Text>
                            </View>


                        )}

                    /> */}


                </View>
            </View >
        )
    }
}

function mapStateToProps(state) {
    return {
        inventorydata: state.inventory.inventorydata.reverse(),
        inventorycounts: state.inventory.inventorycounts,
        vaccinecounts:state.inventory.vaccinecounts,
        Counts: Count(state.inventory.inventorydata)
    }
}

export default connect(mapStateToProps, {})(Inventory_Show_More)