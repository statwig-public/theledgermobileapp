import React from 'react';
import {
    Button, View, Text, StyleSheet, BackHandler, ImageBackground, FlatList,
    Image, ScrollView, TextInput, TouchableOpacity, StatusBar, Animated
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import countries from "../../../Json/countries.json"
import { connect } from 'react-redux';
import { userinfo, upload_Image, user_update, selected_po, create_new_shipmemnt, fetchPublisherShipments } from '../../../Action';
import Picker from 'react-native-picker';
import CountryDropdown from "../../../components/CountryDropdown";
import PODropDown from "../../../components/PODropDown";
import DatePicker from 'react-native-datepicker';
import LinearGradient from 'react-native-linear-gradient';
import PopUp from "../../../components/PopUp";
//import countries from "all-countries-and-cities-json";

class Review_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.state = {
            isModalVisible: false,
            isModalVisible2: false,
            POID: props.navigation.getParam('POID'),
            data: props.navigation.getParam('data'),
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Add_Shipment_Product');
        return true;
    }
    componentDidMount = async () => {
        console.log("Review_Shipment", this.state.POID, this.state.data);
    }
    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('Home_Shipment')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }

    _createshipment = async () => {
        console.log("ckjdns");
        const data = this.state.data
        const result = await this.props.create_new_shipmemnt(data)
        console.log("result ship", result);
        if (result.status === 200) {
            const res = await this.props.fetchPublisherShipments(0,5)
            console.log("fetchPublisherShipments",res);
            if (res.status === 200) {
                this.setState({
                    isModalVisible: true
                })
            }
        } else {
            this.setState({ isModalVisible2: true })
        }


    }

    render() {
        const {
            shipmentId,
            client,
            receiver,
            supplier,
            supplierLocation,
            shipmentDate,
            deliveryTo,
            deliveryLocation,
            estimateDeliveryDate,
            status,
            products
        } = this.state.data
        var date = new Date();
        return (
            <View style={styles.container}>

                {/* {this.props.loder && <LottieView source={require('../../../Json/loading.json')} autoPlay loop />} */}
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Create Shipment'} />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-75) }}>

                    <PopUp isModalVisible={this.state.isModalVisible}
                        image={require('../../../assets/Sucess.png')}
                        text="Success!"
                        text2={"Your Shipment has been created successfully & is on the way!"}
                        label="Ok"
                        Ok={() => this.ok()} />
                    <PopUp isModalVisible={this.state.isModalVisible2}
                        image={require('../../../assets/fail.png')}
                        text="Fail!"
                        text2="Something went wrong. Please try again."
                        label="Try again"
                        Ok={() => this.fail()} />
                    <View style={{ flex:1, width: scale(328), backgroundColor: "#FFFFFF", marginTop: verticalScale(10), borderRadius: 10 }}>
                        <View style={{ marginLeft: scale(50), marginRight: scale(16), marginTop: verticalScale(18) }}>

                            <Text style={{ alignSelf: "flex-end" }}>
                                {date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear()}
                            </Text>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Shipment ID</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{shipmentId}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Client</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{client}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Order ID</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.POID}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Supplier</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{supplier}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Supplier Location</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{supplierLocation}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Shipment Date</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{shipmentDate}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Deliver to</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{deliveryTo}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Delivery Location</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{deliveryLocation}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Delivery Date</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{estimateDeliveryDate}</Text>
                            </View>

                            <View style={{height:scale(10)}}/>
                        </View>
                    </View>

                    {products.map((item, index) => (
                        <View style={{ height: scale(200), width: scale(328), backgroundColor: "#FFFFFF", marginTop: verticalScale(15), borderRadius: 10 }}>
                            <View style={{ marginLeft: scale(10), marginTop: verticalScale(10) }}>
                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                    <View style={{
                                        width: scale(25), borderRadius: 400, height: scale(25), backgroundColor:
                                            item.productName === "BCG" ? "#D9E5EF" : item.productName === "bOPV" ? "#FFBCC4" : item.productName === "OPV" ? "#FFBCC4" : item.productName === "Hep B" ? "#FFEF83" : item.productName === "HepB" ? "#FFEF83" : item.productName === "DTwP" ? "#FFD0CA" : item.productName === "IPV" ? "#EBDDED" : item.productName === "HiB" ? "#F1DDC6" : item.productName === "RV" ? "#D4EEEC" : item.productName === "PVC" ? "#FFC18C" : item.productName === "MMR" ? "#C1E3F2" : "#FF0000",
                                        justifyContent: "center", alignItems: "center"
                                    }}>
                                        <Text style={{ color: "#707070", fontSize: scale(7), fontFamily: "Roboto-Bold", fontWeight: "bold" }}>{item.productName}</Text>
                                    </View>
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(13) }}>{item.productName}</Text>
                                    <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(10) }}></Text>
                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(5) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Manufacturer</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.manufacturerName}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Qty</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.quantity}</Text>
                                </View>
                                {/* <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Mfg Date</Text>

                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.manufacturingDate}</Text>

                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Exp Date</Text>

                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.expiryDate}</Text>

                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Batch No</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.batchNumber}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13), width: "35%" }}>Serial No.</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.serialNumber}</Text>
                                </View> */}
                            </View>
                        </View>

                    ))}

                    <View style={{ flexDirection: "row", width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(20), bottom: 10, justifyContent: "space-between" }}>

                        <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}

                            onPress={() => this.handleBackButtonClick()}>
                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../../assets/edit.png')} />
                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold", marginLeft: scale(10) }}>EDIT</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this._createshipment()}
                            style={{ width: scale(195), height: scale(40), borderRadius: 8, flexDirection: "row", backgroundColor: "#FA7923", alignItems: "center", justifyContent: "space-evenly" }}>
                            <Image style={{ width: scale(13.14), height: scale(15), }}
                                resizeMode='center'
                                source={require('../../../assets/add.png')} />
                            <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>CREATE SHIPMENT</Text>

                        </TouchableOpacity>
                    </View>
                    <View style={{ height: scale(10) }} />

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})
function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        loder: state.loder,
        poid: state.purchaseorder.purchaseorderids

    }
}

export default connect(mapStateToProps, { upload_Image, user_update, selected_po, create_new_shipmemnt, fetchPublisherShipments })(Review_Shipment)
// export default Create_Purchase_Order;