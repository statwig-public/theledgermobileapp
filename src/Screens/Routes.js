import * as React from 'react';
import { Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { scale, moderateScale, verticalScale } from './../components/Scale';
import SplashScreen from "./SplashScreen";
import AuthLoadingScreen from "./AuthLoadingScreen";
import Signin from "./Signin";
import Signup from "./Signup";
import OTP from "./Signup/OTP"
import Shipment from "./Home_Shipment/Shipment";
import Home_Shipment from "./Home_Shipment";
import Shipment_Orders from "./Shipment_Orders";
import Shipment_Order_ShowMore from "../Screens/Shipment_Orders/Shipment_Order_ShowMore";
import Inventory from "./Inventory";
import Inventory_Show_More from "./Inventory/Inventory_Show_More";
import Scan from "./Scan";
import Custom_Side_Menu from "../components/Custom_Side_Menu";
import Profile from "../Screens/Profile";
import Add_Inventory from "../Screens/Inventory/Add_Inventory";
import Create_Purchase_Order from "../Screens/Create_Purchase_Order";
import Review_Inventory from "../Screens/Inventory/Add_Inventory/Review_Inventory";
import Review_Purchase_Order from "../Screens/Create_Purchase_Order/Review_Purchase_Order";
import Shipment_Details from "../Screens/Create_Shipment/Shipment_Details";
import Add_Shipment_Product from "../Screens/Create_Shipment/Add_Shipment_Product";
import Review_Shipment from "../Screens/Create_Shipment/Review_Shipment";
import Purchase_Order from "../Screens/Purchase_Order";
import Send_Shipment from "../Screens/Send_Shipment"

const ShipmentScreen = createStackNavigator({
  Shipment: {
    screen: Shipment,
    navigationOptions: {
      headerShown: false,
    },
  },
  Home_Shipment: {
    screen: Home_Shipment,
    navigationOptions: {
      headerShown: false,
    },
  },
  Create_Purchase_Order: {
    screen: Create_Purchase_Order,
    navigationOptions: {
      headerShown: false,
    },
  },
  Review_Purchase_Order: {
    screen: Review_Purchase_Order,
    navigationOptions: {
      headerShown: false,
    },
  },
  Shipment_Details: {
    screen: Shipment_Details,
    navigationOptions: {
      headerShown: false,
    },
  },
  Add_Shipment_Product: {
    screen: Add_Shipment_Product,
    navigationOptions: {
      headerShown: false,
    },
  },
  Review_Shipment: {
    screen: Review_Shipment,
    navigationOptions: {
      headerShown: false,
    },
  },
  Purchase_Order: {
    screen: Purchase_Order,
    navigationOptions: {
      headerShown: false,
    },
  },
  Send_Shipment:{
    screen: Send_Shipment,
    navigationOptions: {
      headerShown: false,
    },
  }
},
  {
    initialRouteName: 'Home_Shipment',

  })

const Shipment_OrdersScreen = createStackNavigator({
  Shipment_Orders: {
    screen: Shipment_Orders,
    navigationOptions: {
      headerShown: false,
    },
  },
  Shipment_Order_ShowMore: {
    screen: Shipment_Order_ShowMore,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Shipment_Orders',

  })

const InventoryScreen = createStackNavigator({
  Inventory: {
    screen: Inventory,
    navigationOptions: {
      headerShown: false,
    },
  },
  Inventory_Show_More: {
    screen: Inventory_Show_More,
    navigationOptions: {
      headerShown: false,
    },
  },
  Add_Inventory: {
    screen: Add_Inventory,
    navigationOptions: {
      headerShown: false,
    },
  },
  Review_Inventory: {
    screen: Review_Inventory,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Inventory',

  })

const ScanScreen = createStackNavigator({
  Scan: {
    screen: Scan,
    navigationOptions: {
      headerShown: false,
    },
  }
})

const BottomTabNavigator = createBottomTabNavigator({
  ORDERS: {
    screen: Shipment_OrdersScreen
  },
  SHIPMENT: {
    screen: ShipmentScreen
  },
  INVENTORY: {
    screen: InventoryScreen
  },
  SCAN: {
    screen: ScanScreen
  }
}, {
  initialRouteName: 'SHIPMENT',
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'SHIPMENT') {
        return <Image
          style={{ width: scale(21.7), height: scale(16.06), borderWidth: 0, }}
          source={focused
            ? require('../assets/Shipment1.png') : require('../assets/Shipment.png')}
          resizeMode='contain' />;
      } else if (routeName === 'INVENTORY') {
        return <Image
          style={{ width: scale(18), height: scale(18), borderWidth: 0, }}
          source={focused
            ? require('../assets/Inventory1.png') : require('../assets/Inventory.png')}
          resizeMode='contain' />;
      } else if (routeName === 'ORDERS') {
        return <Image
          style={{ width: scale(16.59), height: scale(18), borderWidth: 0, }}
          source={focused
            ? require('../assets/order1.png') : require('../assets/order.png')}
          resizeMode='contain' />;
      } else if (routeName === 'SCAN') {
          return <Image
            style={{ width: scale(18), height: scale(18), borderWidth: 0, }}
            source={focused
              ? require('../assets/Scan1.png') : require('../assets/Scan.png')}
            resizeMode='contain' />;
      }
    },
  }),
  tabBarOptions: {
    activeTintColor: '#0093E9',
    inactiveTintColor: '#A8A8A8',
    style: {
      backgroundColor: '#FFFFFF',
      height: scale(45)
    }
  },
});

const HamburgerNavigation = createDrawerNavigator(

  {
    BottomTabNavigator: {
      screen: BottomTabNavigator,
      navigationOptions: {
        headerShown: false,
        tabBarVisible: false,
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'BottomTabNavigator',
    contentComponent: Custom_Side_Menu,
    drawerWidth: scale(250),
  },
);


const AuthStack = createStackNavigator({
  Signin:
  {
    screen: Signin,
    navigationOptions: {
      headerShown: false,
    },
  }
})

const Root = createSwitchNavigator({
  SplashScreen: SplashScreen,
  AuthLoading: AuthLoadingScreen,
  App: HamburgerNavigation,
  Auth: AuthStack
},
  {
    initialRouteName: 'SplashScreen',
  })

const SignupScreens = createSwitchNavigator({
  Signup: {
    screen: Signup,
    navigationOptions: {
      headerShown: false,
    },
  },
  OTP: {
    screen: OTP,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Signup',
  })

const RootStack = createStackNavigator({
  Root: {
    screen: Root,
    navigationOptions: {
      headerShown: false,
    },
  },
  Signup:
  {
    screen: SignupScreens,
    navigationOptions: {
      headerShown: false,
    },
  },
  Drawer: {
    screen: HamburgerNavigation,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Root',
  })

const AppContainer = createAppContainer(RootStack);

export default class Routes extends React.Component {
  render() {
    return <AppContainer />;
  }
}