import React from 'react';
import {
    Button, View, Text, StyleSheet, BackHandler, ImageBackground, FlatList,
    Image, ScrollView, TextInput, TouchableOpacity, StatusBar, Animated
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import Header from "../../components/Header"
import countries from "../../Json/countries.json"
import { connect } from 'react-redux';
import { userinfo, upload_Image, user_update } from '../../Action';
import Picker from 'react-native-picker';
import CountryDropdown from "../../components/CountryDropdown";
//import countries from "all-countries-and-cities-json";

class Create_Purchase_Order extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.state = {
            isModalVisible: false,
            text: '',
            delivery_to: '',
            destination: '',
            products: [{
                productName: "",
                manufacturerName: "",
                quantity: "",
            }]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home_Shipment');
        return true;
    }
    componentDidMount = async () => {
        console.log("Create_Purchase_Order", countries);
        let data = [];
        for (let key in countries) {
            const Object = {};
            countries[key].forEach((item, index) => {
                data.push(item + ", " + key);
            })

        }
        this.arrayholder = data;
        this.setState({
            countries: data,
            final: data
        })
    }
    _user() {
        let alluserdata = this.props.alluserdata
        let user = [];
        for (var i = 0; i < alluserdata.length; i++) {
            if (this.props.user.name !== alluserdata[i].name) {
                user.push(alluserdata[i].name)
            }
        }
        return user
    }
    _showUserName() {
        Picker.init({
            pickerData: this._user(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Delivery To",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({ delivery_to: pickedValue })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                this.setState({ delivery_to: pickedValue })
            }
        });
        Picker.show();
    }

    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item ? item.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });

        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            countries: newData,
            text: text,
        });
    }

    _showProductName(index) {
        Picker.init({
            pickerData: ['bOPV', 'MMR', 'PVC', 'BCG', 'RV', 'Hep B'],
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Product",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].productName = value
                this.setState({ products: productClone })

            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].productName = value
                this.setState({ products: productClone })
            }
        });
        Picker.show();
    }

    _showManufacturer(index) {
        Picker.init({
            pickerData: ['Bharat Biotech', 'Aroma Biotech', 'Chronly industries', 'GH Pharmas',],
            pickerTextEllipsisLen: 30,
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturer",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturerName = value
                this.setState({ products: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturerName = value
                this.setState({ products: productClone })

            }
        });
        Picker.show();
    }

    _add_another_product() {
        this.setState({
            products: [...this.state.products, {
                productName: "",
                manufacturerName: "",
                quantity: "",
            }]
        })
    }

    cancel = (index) => {
        console.log("hello", index);
        const productClone = JSON.parse(JSON.stringify(this.state.products));
        console.log(Object.keys(productClone));
        productClone.splice(index, 1)
        this.setState({ products: productClone })
    }

    updateValue(index, item) {
        const re = /^[0-9\b]+$/;
        // if (parseInt(item.quantity) > 0) {
        if (parseInt(item.quantity) > 0 && re.test(item.quantity)) {
            this.setState({
                products: [
                    ...this.state.products.slice(0, index),
                    Object.assign({}, this.state.products[index], item),
                    ...this.state.products.slice(index + 1)
                ]
            });
        } else {
            alert('Invalid Quantity')
        }

        console.log('data', this.state.products);
    }

    _SaveProduct = async () => {
        console.log('save', this.state.products);
        console.log('bbbbbb', this.props.inventorydata);
        for (let index in this.state.products) {
            let res = this.state.products[index]
            if (!res.productName || !res.manufacturerName || !res.quantity || !this.state.delivery_to || !this.state.destination) {
                alert('Please fill all the details to Proceed')
                throw new Error('inventory unsuccessful')
            }
        }

        this.props.navigation.navigate('Review_Purchase_Order', {
            products: this.state.products,
            delivery_to: this.state.delivery_to,
            destination: this.state.destination
        })
    }

    render() {
        var Products_Details = [];
        for (let i = 0; i < this.state.products.length; i++) {
            Products_Details.push(
                <View style={{ height: scale(250), width: scale(328), backgroundColor: "#FFFFFF", marginTop: verticalScale(20), borderRadius: 10 }}>
                    <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>

                        <View style={{ flexDirection: "row", height: scale(35), marginTop: verticalScale(20) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Product Name</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showProductName(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].productName === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].productName === "" ? "Select Product" : this.state.products[i].productName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), marginTop: verticalScale(20) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Manufacturer Name</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showManufacturer(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].manufacturerName === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].manufacturerName === "" ? "Select Manufacturer" : this.state.products[i].manufacturerName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Quantity</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Quantity"
                                    keyboardType="number-pad"
                                    value={this.state.products[i].quantity}
                                    onChangeText={(quantity) => this.updateValue(i, { quantity })}
                                />
                            </View>
                        </View>

                        <TouchableOpacity
                            style={{ marginTop: verticalScale(30), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: this.state.products.length === 1 ? "#D6D6D6" : "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}
                            onPress={() => this.cancel(i)}
                            disabled={this.state.products.length === 1 ? true : false}
                        >
                            <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: this.state.products.length === 1 ? "#D6D6D6" : "#0093E9", fontWeight: "bold" }}>CANCEL</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            )

        }
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Create Purchase Order'} />
                <CountryDropdown
                    isModalVisible={this.state.isModalVisible}
                    countries={this.state.countries}
                    text={this.state.text}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onPress={(item) => this.setState({ destination: item, isModalVisible: false, text: '', countries: this.state.final })}
                />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-75) }}>

                    <View style={{ height: scale(175), width: scale(328), backgroundColor: "#FFFFFF", marginTop: verticalScale(10), borderRadius: 10 }}>
                        <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>
                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Supplier</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}>
                                    <Text style={{ width: "85%", color: "#000" }}>{this.props.user.name}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Delivery To</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this._showUserName()}>
                                    <Text style={{ width: "85%", color: this.state.delivery_to === "" ? "#A8A8A8" : "#000" }}>{this.state.delivery_to === "" ? "Select Person" : this.state.delivery_to}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Delivery location</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this.setState({ isModalVisible: true })}>
                                    <Text style={{ width: "85%", color: this.state.destination === "" ? "#A8A8A8" : "#000" }} numberOfLines={1}>{this.state.destination === "" ? "Select Delivery location" : this.state.destination}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>

                    {Products_Details}

                    <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(58), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                        onPress={() => this._add_another_product()}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../assets/plus.png')} />
                        <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Add Another Product</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(40), backgroundColor: "#FFAB1D", borderRadius: 10, alignItems: "center", justifyContent: "center" }}
                        onPress={() => this._SaveProduct()}
                    >
                        <Text style={{ marginLeft: scale(13), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>REVIEW</Text>
                    </TouchableOpacity>

                    <View style={{ height: scale(25) }} />

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})
function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        loder: state.loder,

    }
}

export default connect(mapStateToProps, { upload_Image, user_update })(Create_Purchase_Order)
// export default Create_Purchase_Order;