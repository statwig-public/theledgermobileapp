import React from 'react';
import {
    Button, View, Text, StyleSheet, ScrollView, BackHandler,
    PermissionsAndroid,
    Platform,
    ImageBackground, Image, TouchableOpacity, StatusBar, Animated, Alert
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { add_inventory, Create_Purchase_Order, addInventory } from '../../Action';
import Header from "../../components/Header"
import PopUp from "../../components/PopUp";
import { customisepurchaseorderData } from "../../utils/Purchase_Order_Data";
import LinearGradient from 'react-native-linear-gradient';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';

class Purchase_Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.navigation.getParam('id', 'NO-ID'),
            qrvalue: '',
            opneScanner: false,
            scan: false,
            scanObj: {}
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home_Shipment');
        return true;
    }
    componentDidMount = async () => {
        console.log("Review Purchase Order", this.state.id);
    }

    create_shipment = () => {
        const { id } = this.state
        this.props.navigation.navigate('Shipment_Details', { id });
    }

    reveiveShipment = async () => {
        var final_data = {}
        var product = {}
        const {scanObj} = this.state;
        this.props.purchaseorder.find(ele =>
            this.state.id === ele.orderID ?
                final_data = ele : null)
        console.log("final", final_data);
        const data = {
            serialNumberRange: scanObj?.serialNumberRange,
            manufacturingDate: scanObj?.manufacturingDate,
            expiryDate: scanObj?.expiryDate,
            productName: Object.keys(final_data.products[`0`])[0].split('-')[0],
            poNumber: final_data.orderID,
            shipmentId: scanObj?.shipmentNumber,
            manufacturerName:Object.keys(final_data.products[`0`])[0].split('-')[1], 
            batchNumber:scanObj?.batchNumber
        }
        product[`productName`] = Object.keys(final_data.products[`0`])[0].split('-')[0];
        product[`quantity`] = final_data.total;
        product[`manufacturerName`] = Object.keys(final_data.products[`0`])[0].split('-')[1];
        product[`storageConditionmin`] = scanObj?.storageConditionmin;
        product[`storageConditionmax`] = scanObj?.storageConditionmax;
        product[`manufacturingDate`] = scanObj?.manufacturingDate;
        product[`expiryDate`] = scanObj?.expiryDate;
        product[`batchNumber`] = scanObj?.batchNumber;
        product[`serialNumber`] = scanObj?.serialNumberRange;
console.log("product",product);
        const data2 = {
            shipmentId: scanObj?.shipmentNumber,
            client: final_data.clientId,
            supplier: final_data.vendorName,
            supplierLocation: final_data.incoterms2,
            shipmentDate: '5/12/2020',
            deliveryTo: final_data.destination,
            deliveryLocation: final_data.destination,
            estimateDeliveryDate: '31/12/2020',
            poNumber: final_data.orderID,
            //batchNumber: scanObj?.batchNumber,
            //serialNumberRange: scanObj?.serialNumberRange,
            products: [product],
            status: 'Received',
            //sender: final_data?.sendpoto?.address,
            receiver: final_data?.receiver,
            //quantity: final_data.total
        }
        console.log("insert inventry", data);
        console.log("creat shipment",data2);
    await this.props.addInventory(data, data2, this.props.navigation.navigate('Home_Shipment'))
    }

    onOpneScanner() {
        var that = this;
        //To Start Scanning
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'CameraExample App Camera Permission',
                        'message': 'CameraExample App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //If CAMERA Permission is granted
                        that.setState({ qrvalue: '' });
                        that.setState({ opneScanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            //Calling the camera permission function
            requestCameraPermission();
        } else {
            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
        }
    }

    AsyncAlert(message) {
        return new Promise((resolve, reject) => {
            Alert.alert(
                '',
                message,
                [
                    { text: 'OK', onPress: () => resolve('YES') },
                ],
                { cancelable: false }
            )
        })
    }
    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    onBarcodeScan(qrvalue) {
        //called after te successful scanning of QRCode/Barcode
        console.log('qr', qrvalue);
        this.setState({ opneScanner: false })
        if (!this.isJson(qrvalue)) {
            this.AsyncAlert("invalid QR Code").then(() => {
                this.setState({ opneScanner: true })
            })
            return
        } else {
            //this.setState({  })
            console.log('json', JSON.parse(qrvalue));
            const value = JSON.parse(qrvalue);
            this.setState({
                scanObj: value, scan: true
            })
        }




    }

    render() {
        console.log("this.props.purchaseorder", this.props.purchaseorder);

        const {
            id,
            supplier
        } = this.props.purchaseorder
        const { scanObj } = this.state;
        if (!this.state.opneScanner) {
            return (
                <View style={{
                    flex: 1,
                    alignItems: "center",
                }}>
                    <StatusBar backgroundColor="#0093E9" />
                    <Header name={'View Purchase Order'} />
                    <ScrollView nestedScrollEnabled={true}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-70) }}>
                        {
                            this.props.purchaseorder.map((item, index) => (
                                <>
                                    {this.state.id === item.orderID ?
                                        <View style={{ width: scale(328), height: scale(270), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(14) }}>
                                            <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                                                <View style={{ flexDirection: "row", }}>
                                                    <View style={{ marginTop: verticalScale(10) }}>
                                                        <Image style={{ width: scale(27), height: scale(27) }}
                                                            resizeMode='center'
                                                            source={require('../../assets/user.png')} />
                                                    </View>
                                                    <View style={{ marginLeft: scale(7) }}>
                                                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }} numberOfLines={1}>{item.sendpoto?.name}</Text>
                                                        <View style={{ width: scale(58), height: scale(22), marginTop: verticalScale(5), backgroundColor: "#3CB049", justifyContent: "center", alignItems: "center", borderRadius: 12 }}>
                                                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Bold", color: "#fff", }}>Approved</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), right: 0, position: "absolute" }}>
                                                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{item.date}</Text>

                                                    </View>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Purchase order ID</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#000000", }}>{item.orderID}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Purchase order Item #</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.poItem}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Shipped From</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.incoterms2}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Vendor ID</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.vendor}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Vendor Name</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.vendorName}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>To Location ID</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.plant}</Text>
                                                </View>
                                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>To Location</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.destination}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        : null}
                                </>
                            ))
                        }

                        {this.props.purchaseorder.map((item, index) => (
                            <>
                                {this.state.id === item.orderID ?
                                    Object.keys(this.props.purchaseorder[index].products).map((key, index2) => (
                                        <View style={{ width: scale(328), flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20) }}>
                                            <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                                                <View style={{ flexDirection: "row", alignItems: "center", }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Product Name</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{Object.keys(this.props.purchaseorder[index].products[key])[0].split('-')[0]}</Text>
                                                </View>
                                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Manufacturer</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{Object.keys(this.props.purchaseorder[index].products[key])[0].split('-')[1]}</Text>
                                                </View>
                                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Quantity</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.props.purchaseorder[index].products[key][`${Object.keys(this.props.purchaseorder[index].products[key])[0].split('-')[0]}-${Object.keys(this.props.purchaseorder[index].products[key])[0].split('-')[1]}`]}</Text>
                                                </View>
                                                {this.state.scan ?
                                                    <>
                                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Mfg Date</Text>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{scanObj?.manufacturingDate}</Text>
                                                        </View>
                                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Exp Date</Text>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{scanObj?.expiryDate} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Batch No</Text>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{scanObj?.batchNumber} </Text>
                                                        </View>
                                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Serial No.</Text>
                                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{scanObj?.serialNumberRange} </Text>
                                                        </View>
                                                    </> : null }
                                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }} />
                                            </View>
                                        </View>
                                    ))

                                    : null}
                            </>
                        ))}

<View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(60) }} />
                    </ScrollView>

                    {this.state.scan ?
                        <View style={{ flexDirection: "row", width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(90), position: 'absolute', bottom: 10, justifyContent: "space-between" }}>

                            <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}
                                disabled={true}>
                                <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                    resizeMode='center'
                                    source={require('../../assets/edit.png')} />
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold", marginLeft: scale(10) }}>EDIT</Text>
                            </TouchableOpacity>


                            <TouchableOpacity onPress={() => { this.reveiveShipment() }}
                                style={{ width: scale(195), height: scale(40), borderRadius: 8, flexDirection: "row", backgroundColor: "#FA7923", alignItems: "center", justifyContent: "space-evenly" }}>
                                <Image style={{ width: scale(15), height: scale(15), }}
                                    resizeMode='center'
                                    source={require('../../assets/Receiveshipment.png')} />
                                <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>RECEIVE SHIPMENT</Text>

                            </TouchableOpacity>
                        </View>
                        :
                        <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(46), backgroundColor: "#FA7923", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center", bottom: verticalScale(10) }}
                            onPress={() => this.onOpneScanner()} >

                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../assets/Receiveshipment.png')} />
                            <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>RECEIVE SHIPMENT</Text>

                        </TouchableOpacity>}
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <CameraKitCameraScreen
                    showFrame={true}
                    //Show/hide scan frame
                    scanBarcode={true}
                    //Can restrict for the QR Code only
                    laserColor={'green'}
                    //Color can be of your choice
                    frameColor={'green'}
                    //If frame is visible then frame color
                    colorForScannerFrame={'black'}
                    //Scanner Frame color
                    onReadCode={event =>
                        this.onBarcodeScan(event.nativeEvent.codeStringValue)
                    }
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})

function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        inventorydata: state.inventory.inventorydata,
        purchaseorder: customisepurchaseorderData(state.purchaseorder.purchaseorder, "Purchase_Order"),
    }
}

export default connect(mapStateToProps, { add_inventory, Create_Purchase_Order, addInventory })(Purchase_Order)

//export default Review_Inventory;