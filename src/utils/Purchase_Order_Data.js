export const customisepurchaseorderData = (purchaseorder, name) => {
    if (name === "HomeShipment") {
        let data = [];
        for (let i in purchaseorder) {
            const purchaseorderObject = {};
            var sum = 0;
            if (typeof purchaseorder[i].products == 'object') {
                for (let key in purchaseorder[i].products) {
                    Object.keys(purchaseorder[i].products[key]).forEach(element => {
                        sum += parseFloat(purchaseorder[i].products[key][element])
                    });
                }
                purchaseorderObject = purchaseorder[i]
                //purchaseorderObject['id'] = id[i]
                purchaseorderObject['total'] = sum
                data.push(purchaseorderObject);
            }
        }

        return data.reverse();
    } else if (name === "Purchase_Order") {
        let data = [];
        for (let i in purchaseorder) {
            const purchaseorderObject = {};
            // var sum = 0;
            // if (typeof purchaseorder[i].products == 'object') {
            //     for (let key in purchaseorder[i].products) {
            //         Object.keys(purchaseorder[i].products[key]).forEach(element => {
            //             sum += parseFloat(purchaseorder[i].products[key][element])
            //         });
            //     }
            purchaseorderObject = purchaseorder[i]
            //purchaseorderObject['id'] = id[i]
            // purchaseorderObject['total'] = sum
            data.push(purchaseorderObject);
        }

        return data.reverse();
    }
}
