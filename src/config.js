export const STABLE_SERVER_URL_USER = 'http://52.90.57.31:3001';
export const STABLE_SERVER_URL_SHIPMENT = 'http://52.90.57.31:3002';
export const STABLE_SERVER_URL_INVENTORY = 'http://52.90.57.31:3007';
export const TEST_SERVER_URL = 'http://test.vaccineledger.com:9001';
export const PROD_SERVER_URL = 'http://api.vaccineledger.com:9001';
export const CLOUDLEAF_SERVER_URL = 'http://cloudleaf.vaccineledger.com:9001';

export function config() {
    const confs = {

stable: {
    loginUrl: `${STABLE_SERVER_URL_USER}/api/auth/login`,
    registerUrl: `${STABLE_SERVER_URL_USER}/api/auth/register`,
    verifyOtpUrl: `${STABLE_SERVER_URL_USER}/api/auth/verify-otp`,
    userInfoUrl: `${STABLE_SERVER_URL_USER}/api/auth/userInfo`,
    getAllUsersUrl: `${STABLE_SERVER_URL_USER}/api/auth/getAllUsers`,
    updateProfileUrl: `${STABLE_SERVER_URL_USER}/api/auth/updateProfile`,
    upload: `${STABLE_SERVER_URL_USER}/api/auth/upload`,
    shipmentsUrl: `${STABLE_SERVER_URL_SHIPMENT}/api/shipping/fetchPublisherLatestShipments`,
    inventoriesUrl: `${STABLE_SERVER_URL_INVENTORY}/api/inventory/getAllInventoryDetails`,
    createShipmentUrl: `${STABLE_SERVER_URL_SHIPMENT}/api/shipping/createShipment?address=`,
    addInventoryUrl: `${STABLE_SERVER_URL_INVENTORY}/api/inventory/addNewInventory`,
    shipmentsSearch: `${STABLE_SERVER_URL_SHIPMENT}/api/shipping/fetchShipments?key=`,
    inventorySearch: `${STABLE_SERVER_URL_INVENTORY}/api/inventory/getInventoryDetailsForProduct?key=`
  },
  test: {
    loginUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/login`,
    registerUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/register`,
    verifyOtpUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/verify-otp`,
    userInfoUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/userInfo`,
    getAllUsersUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/getAllUsers`,
    updateProfileUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/updateProfile`,
    upload: `${TEST_SERVER_URL}/usermanagement/api/auth/upload`,
    shipmentsUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchUserShipments`,
    createShipmentUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createShipment`,
    createShipmentUrladdress: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createShipment?address=`,
    shipmentsSearch: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchShipments?key=`,
    createPurchaseOrderUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createPurchaseOrder`,
    fetchAllPurchaseOrdersUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchPublisherPurchaseOrders`,
    fetchAllPurchaseOrderUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchpurchaseOrder?key=`,
    fetchPurchaseOrderStatisticsUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/purchaseOrderStatistics`,
    inventorySearch: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/getInventoryDetailsForProduct?key=`,
    inventoriesUrl: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/getAllInventoryDetails`,
    addInventoryUrl: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/addNewInventory`,
    addInventory:`${TEST_SERVER_URL}/inventorymanagement/api/inventory/insertInventories`,
    updateInventory:`${TEST_SERVER_URL}/inventorymanagement/api/inventory/updateInventories`
  },
  // test: {
  //   loginUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/login`,
  //   registerUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/register`,
  //   verifyOtpUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/verify-otp`,
  //   userInfoUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/userInfo`,
  //   getAllUsersUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/getAllUsers`,
  //   updateProfileUrl: `${TEST_SERVER_URL}/usermanagement/api/auth/updateProfile`,
  //   upload: `${TEST_SERVER_URL}/usermanagement/api/auth/upload`,
  //   shipmentsUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchPublisherLatestShipments`,
  //   createShipmentUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createShipment`,
  //   createShipmentUrladdress: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createShipment?address=`,
  //   shipmentsSearch: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchShipments?key=`,
  //   createPurchaseOrderUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/createPurchaseOrder`,
  //   fetchAllPurchaseOrdersUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchAllPurchaseOrders`,
  //   fetchAllPurchaseOrderUrl: `${TEST_SERVER_URL}/shipmentmanagement/api/shipping/fetchpurchaseOrder?key=`,
  //   inventorySearch: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/getInventoryDetailsForProduct?key=`,
  //   inventoriesUrl: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/getAllInventoryDetails`,
  //   addInventoryUrl: `${TEST_SERVER_URL}/inventorymanagement/api/inventory/addNewInventory`,

  // },
  prod: {
    loginUrl: `${PROD_SERVER_URL}/usermanagement/api/auth/login`,
    registerUrl: `${PROD_SERVER_URL}/usermanagement/api/auth/register`,
    verifyOtpUrl: `${PROD_SERVER_URL}/usermanagement/api/auth/verify-otp`,
    userInfoUrl: `${PROD_SERVER_URL}/usermanagement/api/auth/userInfo`,
    getAllUsersUrl: `${STABLE_SERVER_URL_USER}/api/auth/getAllUsers`,
    updateProfileUrl: `${PROD_SERVER_URL}/usermanagement/api/auth/updateProfile`,
    upload: `${PROD_SERVER_URL}/usermanagement/api/auth/upload`,
    shipmentsUrl: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/fetchPublisherLatestShipments`,
    createShipmentUrl: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/createShipment`,
    shipmentsSearch: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/fetchShipments?key=`,
    createPurchaseOrderUrl: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/createPurchaseOrder`,
    fetchAllPurchaseOrdersUrl: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/fetchAllPurchaseOrders`,
    fetchAllPurchaseOrderUrl: `${PROD_SERVER_URL}/shipmentmanagement/api/shipping/fetchpurchaseOrder?key=`,
    inventorySearch: `${PROD_SERVER_URL}/inventorymanagement/api/inventory/getInventoryDetailsForProduct?key=`,
    inventoriesUrl: `${PROD_SERVER_URL}/inventorymanagement/api/inventory/getAllInventoryDetails`,
    addInventoryUrl: `${PROD_SERVER_URL}/inventorymanagement/api/inventory/addNewInventory`,
  },
  cloudleaf: {
    loginUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/login`,
    registerUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/register`,
    verifyOtpUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/verify-otp`,
    userInfoUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/userInfo`,
    getAllUsersUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/getAllUsers`,
    updateProfileUrl: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/updateProfile`,
    upload: `${CLOUDLEAF_SERVER_URL}/usermanagement/api/auth/upload`,
    shipmentsUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/fetchUserShipments`,
    createShipmentUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/createShipment`,
    createShipmentUrladdress: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/createShipment?address=`,
    shipmentsSearch: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/fetchShipments?key=`,
    createPurchaseOrderUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/createPurchaseOrder`,
    fetchAllPurchaseOrdersUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/fetchPublisherPurchaseOrders`,
    fetchAllPurchaseOrderUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/fetchpurchaseOrder?key=`,
    fetchPurchaseOrderStatisticsUrl: `${CLOUDLEAF_SERVER_URL}/shipmentmanagement/api/shipping/purchaseOrderStatistics`,
    inventorySearch: `${CLOUDLEAF_SERVER_URL}/inventorymanagement/api/inventory/getInventoryDetailsForProduct?key=`,
    inventoriesUrl: `${CLOUDLEAF_SERVER_URL}/inventorymanagement/api/inventory/getAllInventoryDetails`,
    addInventoryUrl: `${CLOUDLEAF_SERVER_URL}/inventorymanagement/api/inventory/addNewInventory`,
  }

}

//const conf = confs['stable'];
const conf = confs['test'];
//const conf = confs['cloudleaf'];
//const conf = confs['prod'];

  return conf;
}